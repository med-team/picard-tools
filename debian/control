Source: picard-tools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Olivier Sallou <osallou@debian.org>,
           Vincent Danjean <vdanjean@debian.org>,
           Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: default-jdk,
               debhelper-compat (= 13),
               javahelper,
               gradle-debian-helper,
               maven-repo-helper,
               libguava-java,
               libcommons-collections4-java,
               libcommons-io-java,
               libcommons-lang-java,
               libcommons-lang3-java,
               libcommons-math3-java,
               libbarclay-java,
               libgkl-java,
               libgkl-jni [amd64],
               libgatk-native-bindings-java,
# htsjdk and picard-tools are relased nearly together
               libhtsjdk-java (>= 3.0.2+dfsg),
               libhttp-nio-java,
               libjoptsimple-java,
               libmjson-java,
# required for tests:
               testng,
               r-base-core,
# required for links and dependencies in documentation:
               default-jdk-doc,
               libhtsjdk-java-doc,
               libjs-jquery
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/picard-tools
Vcs-Git: https://salsa.debian.org/med-team/picard-tools.git
Homepage: https://broadinstitute.github.io/picard/
Rules-Requires-Root: no

Package: picard-tools
Architecture: all
Depends: default-jre | java6-runtime,
         ${misc:Depends},
         libpicard-java (= ${binary:Version})
Description: Command line tools to manipulate SAM and BAM files
 SAM (Sequence Alignment/Map) format is a generic format for storing
 large nucleotide sequence alignments. Picard Tools includes these
 utilities to manipulate SAM and BAM files:
  AddCommentsToBam                  FifoBuffer
  AddOrReplaceReadGroups            FilterSamReads
  BaitDesigner                      FilterVcf
  BamIndexStats                     FixMateInformation
                                    GatherBamFiles
  BedToIntervalList                 GatherVcfs
  BuildBamIndex                     GenotypeConcordance
  CalculateHsMetrics                IlluminaBasecallsToFastq
  CalculateReadGroupChecksum        IlluminaBasecallsToSam
  CheckIlluminaDirectory            LiftOverIntervalList
  CheckTerminatorBlock              LiftoverVcf
  CleanSam                          MakeSitesOnlyVcf
  CollectAlignmentSummaryMetrics    MarkDuplicates
  CollectBaseDistributionByCycle    MarkDuplicatesWithMateCigar
  CollectGcBiasMetrics              MarkIlluminaAdapters
  CollectHiSeqXPfFailMetrics        MeanQualityByCycle
  CollectIlluminaBasecallingMetrics MergeBamAlignment
  CollectIlluminaLaneMetrics        MergeSamFiles
  CollectInsertSizeMetrics          MergeVcfs
  CollectJumpingLibraryMetrics      NormalizeFasta
  CollectMultipleMetrics            PositionBasedDownsampleSam
  CollectOxoGMetrics                QualityScoreDistribution
  CollectQualityYieldMetrics        RenameSampleInVcf
  CollectRawWgsMetrics              ReorderSam
  CollectRnaSeqMetrics              ReplaceSamHeader
  CollectRrbsMetrics                RevertOriginalBaseQualitiesAndAddMateCigar
  CollectSequencingArtifactMetrics  RevertSam
  CollectTargetedPcrMetrics         SamFormatConverter
  CollectVariantCallingMetrics      SamToFastq
  CollectWgsMetrics                 ScatterIntervalsByNs
  CompareMetrics                    SortSam
  CompareSAMs                       SortVcf
  ConvertSequencingArtifactToOxoG   SplitSamByLibrary
  CreateSequenceDictionary          SplitVcfs
  DownsampleSam                     UpdateVcfSequenceDictionary
  EstimateLibraryComplexity         ValidateSamFile
  ExtractIlluminaBarcodes           VcfFormatConverter
  ExtractSequences                  VcfToIntervalList
  FastqToSam                        ViewSam

Package: libpicard-java
Architecture: all
Section: java
Depends: ${misc:Depends},
# Getting versionned depends from Build-Depends
# This avoid mismatch, but each library must be extracted in debian/rules
         ${bd:libguava-java},
         ${bd:libhtsjdk-java},
         libbarclay-java,
         libgkl-java,
         libcommons-lang3-java,
         libcommons-math3-java,
         libgatk-native-bindings-java
# avoid ${java:Depends} that contains openjdk-8-jdk-headless
# due to tools.jar in classpath
Recommends: ${java:Recommends},
            r-base-core
Suggests: picard-tools
Description: Java library to manipulate SAM and BAM files
 SAM (Sequence Alignment/Map) format is a generic format for storing
 large nucleotide sequence alignments. This library provides classes to
 manipulate SAM and BAM files.
 .
 A command line wrapper for this library is provided in the picard-tools
 package.

Package: libpicard-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${java:Depends},
         libjs-jquery
Recommends: ${java:Recommends}
Description: Documentation for the java picard library
 SAM (Sequence Alignment/Map) format is a generic format for storing
 large nucleotide sequence alignments. The picard java library provides classes
 to manipulate SAM and BAM files.
 .
 This package contains the javadoc of the picard java library.
