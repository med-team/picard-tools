Description: fix data provider requirements
Author: Sascha Steinbiss <satta@debian.org>
--- a/src/test/java/picard/analysis/directed/CollectHsMetricsTest.java
+++ b/src/test/java/picard/analysis/directed/CollectHsMetricsTest.java
@@ -31,33 +31,33 @@
 
     @DataProvider(name = "collectHsMetricsDataProvider")
     public Object[][] targetedIntervalDataProvider() {
-        final String referenceFile = TEST_DIR + "/chrM.fasta";
-        final String intervals = TEST_DIR + "/chrM.interval_list";
-        final String halfIntervals = TEST_DIR + "/chrM_100bp.interval_list";
-        final String twoSmallIntervals = TEST_DIR + "/two-small.interval_list";
+        final String referenceFile = TEST_DIR.getAbsolutePath() + "/chrM.fasta";
+        final String intervals = TEST_DIR.getAbsolutePath() + "/chrM.interval_list";
+        final String halfIntervals = TEST_DIR.getAbsolutePath() + "/chrM_100bp.interval_list";
+        final String twoSmallIntervals = TEST_DIR.getAbsolutePath() + "/two-small.interval_list";
 
         return new Object[][] {
                 // two reads, each has 100 bases. bases in one read are medium quality (20), in the other read poor quality (10).
                 // test that we exclude half of the bases
-                {TEST_DIR + "/lowbaseq.sam", intervals, 1, 10, true, 2, 200, 0.5, 0.0, 0.0, 0.50, 0.0, 1, 0, 200, 1000},
+                {TEST_DIR.getAbsolutePath() + "/lowbaseq.sam", intervals, 1, 10, true, 2, 200, 0.5, 0.0, 0.0, 0.50, 0.0, 1, 0, 200, 1000},
                 // test that read 2 (with mapping quality 1) is filtered out with minimum mapping quality 2
-                {TEST_DIR + "/lowbaseq.sam", halfIntervals, 1, 10, true, 2, 200, 0.5, 0.0, 0.0, 1.0, 0.0, 1, 1, 200, 1000},
+                {TEST_DIR.getAbsolutePath() + "/lowbaseq.sam", halfIntervals, 1, 10, true, 2, 200, 0.5, 0.0, 0.0, 1.0, 0.0, 1, 1, 200, 1000},
                 // test that read 2 (with mapping quality 1) is filtered out with minimum mapping quality 2 with an interval that is completely covered
-                {TEST_DIR + "/lowmapq.sam", intervals, 2, 0, true, 2, 202, 0, 0.0, 0.0, 0.505, 0.0, 1, 0, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/lowmapq.sam", intervals, 2, 0, true, 2, 202, 0, 0.0, 0.0, 0.505, 0.0, 1, 0, 202, 1000},
                 // test that we clip overlapping bases
-                {TEST_DIR + "/lowmapq.sam", halfIntervals, 2, 0, true, 2, 202, 0, 0.0, 0.00495, 1.0, 0.0, 1, 1, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/lowmapq.sam", halfIntervals, 2, 0, true, 2, 202, 0, 0.0, 0.00495, 1.0, 0.0, 1, 1, 202, 1000},
                 // test that we clip overlapping bases with an interval that is completely covered
-                {TEST_DIR + "/overlapping.sam", intervals, 0, 0, true, 2, 202, 0, 0.5, 0.0, 0.505, 0, 1, 0, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/overlapping.sam", intervals, 0, 0, true, 2, 202, 0, 0.5, 0.0, 0.505, 0, 1, 0, 202, 1000},
                 // test that we do not clip overlapping bases
-                {TEST_DIR + "/overlapping.sam", intervals, 0, 0, false, 2, 202, 0, 0.0, 0.0, 0.505, 0.505, 2, 0, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/overlapping.sam", intervals, 0, 0, false, 2, 202, 0, 0.0, 0.0, 0.505, 0.505, 2, 0, 202, 1000},
                 // test that we exclude half of the bases (due to poor quality) with an interval that is completely covered
-                {TEST_DIR + "/overlapping.sam", halfIntervals, 0, 0, true, 2, 202, 0, 0.5, 0.00495, 1.0, 0, 1, 1, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/overlapping.sam", halfIntervals, 0, 0, true, 2, 202, 0, 0.5, 0.00495, 1.0, 0, 1, 1, 202, 1000},
                 // test that we do not clip overlapping bases with an interval that is completely covered
-                {TEST_DIR + "/overlapping.sam", halfIntervals, 0, 0, false, 2, 202, 0, 0.0, 0.009901, 1.0, 1.0, 2, 2, 202, 1000},
+                {TEST_DIR.getAbsolutePath() + "/overlapping.sam", halfIntervals, 0, 0, false, 2, 202, 0, 0.0, 0.009901, 1.0, 1.0, 2, 2, 202, 1000},
                 // A read 10 base pairs long. two intervals: one maps identically to the read, other does not overlap at all
-                {TEST_DIR + "/single-short-read.sam", twoSmallIntervals, 20, 20, true, 1, 10, 0.0, 0.0, 0.0, 0.5, 0.0, 1, 0, 10, 1000},
+                {TEST_DIR.getAbsolutePath() + "/single-short-read.sam", twoSmallIntervals, 20, 20, true, 1, 10, 0.0, 0.0, 0.0, 0.5, 0.0, 1, 0, 10, 1000},
                 // test that we can figure out low quality and off target in the same bam (low quality is identified first)
-                {TEST_DIR + "/someLowbaseq.sam", twoSmallIntervals, 0, 21, true, 2, 200, 150D / 200, 0D, 40D / 200, 1 / 2D, 0D, 1, 0, 200, 1000},
+                {TEST_DIR.getAbsolutePath() + "/someLowbaseq.sam", twoSmallIntervals, 0, 21, true, 2, 200, 150D / 200, 0D, 40D / 200, 1 / 2D, 0D, 1, 0, 200, 1000},
         };
     }
 
@@ -99,7 +99,7 @@
                                         final long pfBases,
                                         final int sampleSize) throws IOException {
 
-        final File outfile = File.createTempFile("CollectHsMetrics", ".hs_metrics", TEST_DIR);
+        final File outfile = File.createTempFile("CollectHsMetrics", ".hs_metrics", TEST_DIR.getAbsolutePath());
         outfile.deleteOnExit();
 
         final String[] args = new String[] {
@@ -141,14 +141,14 @@
          *  Test that the depth histogram is [10,10,0,...,0]
          */
 
-        final String input = TEST_DIR + "/single-short-read.sam";
-        final String targetIntervals = TEST_DIR + "/two-small.interval_list";
+        final String input = TEST_DIR.getAbsolutePath() + "/single-short-read.sam";
+        final String targetIntervals = TEST_DIR.getAbsolutePath() + "/two-small.interval_list";
         final int minimumMappingQuality = 20;
         final int minimumBaseQuality = 20;
         final boolean clipOverlappingReads = true;
         final int sampleSize = 10;
 
-        final File outfile = File.createTempFile("testCoverageHistogram", ".hs_metrics", TEST_DIR);
+        final File outfile = File.createTempFile("testCoverageHistogram", ".hs_metrics", TEST_DIR.getAbsolutePath());
         outfile.deleteOnExit();
 
         final String[] args = new String[] {
--- a/src/test/java/picard/util/IntervalListToolsTest.java
+++ b/src/test/java/picard/util/IntervalListToolsTest.java
@@ -204,7 +204,6 @@
         };
     }
 
-    @Test(dataProvider = "actionAndTotalBasesWithUniqueData")
     public void testActionsWithUnique(final IntervalListTools.Action action, final long bases, final int intervals) throws IOException {
         final IntervalList il = tester(action, false, true, false, scatterable, secondInput, false);
         Assert.assertEquals(il.getBaseCount(), bases, "unexpected number of bases found.");
--- a/src/test/java/picard/fingerprint/FingerprintCheckerTest.java
+++ b/src/test/java/picard/fingerprint/FingerprintCheckerTest.java
@@ -109,7 +109,6 @@
         };
     }
 
-    @Test(dataProvider = "checkFingerprintsVcfDataProvider")
     public void testCheckFingerprintsVcf(final File vcfFile, final File genotypesFile, final String observedSampleAlias, final String expectedSampleAlias,
                                          final double llExpectedSample, final double llRandomSample, final double lodExpectedSample) throws IOException {
         final Path indexedInputVcf = VcfTestUtils.createTemporaryIndexedVcfFromInput(vcfFile, "fingerprintcheckertest.tmp.").toPath();
@@ -131,7 +130,6 @@
         Assert.assertEquals(mr.getLOD(), lodExpectedSample, DELTA);
     }
 
-    @Test(dataProvider = "checkFingerprintsVcfDataProvider")
     public void testFingerprintVcf(final File vcfFile, final File genotypesFile, final String observedSampleAlias, final String expectedSampleAlias,
                                    final double llExpectedSample, final double llRandomSample, final double lodExpectedSample) {
         final FingerprintChecker fpChecker = new FingerprintChecker(SUBSETTED_HAPLOTYPE_DATABASE_FOR_TESTING);
@@ -140,7 +138,6 @@
         Assert.assertFalse(fp1.isEmpty());
     }
 
-    @Test(dataProvider = "checkFingerprintsVcfDataProvider")
     public void testFingerprintSwapEqual(final File vcfFile, final File genotypesFile, final String observedSampleAlias, final String expectedSampleAlias,
                                          final double llExpectedSample, final double llRandomSample, final double lodExpectedSample) {
         final FingerprintChecker fpChecker = new FingerprintChecker(SUBSETTED_HAPLOTYPE_DATABASE_FOR_TESTING);
@@ -555,4 +552,4 @@
 
         VcfTestUtils.assertVcfFilesAreEqual(vcfOutput, vcfExpected);
     }
-}
\ No newline at end of file
+}
--- a/src/test/java/picard/arrays/illumina/InfiniumDataFileTest.java
+++ b/src/test/java/picard/arrays/illumina/InfiniumDataFileTest.java
@@ -21,13 +21,11 @@
         };
     }
 
-    @Test(dataProvider = "byteArrayToIntDataProvider")
     public void testByteArrayToInt(byte[] bytes, int expectedValue) {
         short value = (short) InfiniumDataFile.byteArrayToInt(bytes);
         Assert.assertEquals(value, expectedValue);
     }
 
-    @Test(dataProvider = "byteArrayToIntDataProvider")
     public void testShortToByteArray(byte[] expectedBytes, int value) {
         byte[] themBytes = InfiniumDataFile.shortToByteArray((short) value);
         Assert.assertEquals(themBytes, expectedBytes);
