Description: Modifies the upstream build system.
Author: Vincent Danjean <Vincent.Danjean@ens-lyon.org>
Forwarded: not-needed
Last-Updated: 2016-07-07

--- a/build.gradle
+++ b/build.gradle
@@ -10,18 +10,10 @@
     id "java-library"
     id 'maven-publish'
     id 'signing'
-    id 'jacoco'
     id 'application'
-    id 'com.palantir.git-version' version '0.5.1'
-    id 'com.github.johnrengelman.shadow' version '8.1.1'
-    id "com.github.kt3k.coveralls" version '2.6.3'
-    id "org.ajoberstar.grgit" version "5.2.0"
-    id "org.ajoberstar.git-publish" version "2.1.1"
 }
 
-application {
-    mainClass = "picard.cmdline.PicardCommandLine"
-}
+mainClassName = "picard.cmdline.PicardCommandLine"
 
 repositories {
     mavenCentral()
@@ -57,8 +49,6 @@
     }
 }
 
-ensureBuildPrerequisites(buildPrerequisitesMessage)
-
 final htsjdkVersion = System.getProperty('htsjdk.version', '4.1.3')
 final log4jVersion = System.getProperty('log4j.version', '2.20.0')
 
@@ -86,41 +76,46 @@
     implementation 'org.broadinstitute:http-nio:1.1.1'
 
     testImplementation 'org.testng:testng:7.7.0'
-
-    constraints {
-        //log4j api and core, specify these so we don't transitively get old vulnerable versions
-        implementation 'org.apache.logging.log4j:log4j-api:' +log4jVersion
-        implementation 'org.apache.logging.log4j:log4j-core:' + log4jVersion
-    }
 }
 
-configurations.configureEach {
+configurations.all {
     resolutionStrategy {
         // force the htsjdk version so we don't get a different one transitively
         force 'com.github.samtools:htsjdk:' + htsjdkVersion
     }
 }
 
-java {
-    toolchain {
-        languageVersion = JavaLanguageVersion.of(17)
+def getDebianVersion() {
+    def dpkgStdOut = new ByteArrayOutputStream()
+    exec {
+        commandLine "dpkg-parsechangelog", "-S", "Version"
+        standardOutput = dpkgStdOut
     }
-    withJavadocJar()
-    withSourcesJar()
+    return dpkgStdOut.toString().trim().replaceFirst(/(-gradle)?([+]dfsg[.0-9]*)?-[^-]+$/, "")
 }
 
 final isRelease = Boolean.getBoolean("release")
-final gitVersion = gitVersion().replaceAll(".dirty", "")
-version = isRelease ? gitVersion : gitVersion + "-SNAPSHOT"
+final debianVersion = getDebianVersion()
+version = debianVersion
 
 logger.info("build for version:" + version)
 group = 'com.github.broadinstitute'
 
 defaultTasks 'all'
 
-tasks.register('all') { dependsOn 'jar', 'distZip', 'javadoc', 'shadowJar', 'currentJar', 'picardDoc' }
+task all { dependsOn 'jar', 'distZip', 'javadoc', 'shadowJar', 'currentJar', 'picardDoc' }
+
+compileJava {
+    sourceCompatibility = 17
+    targetCompatibility = 17
+}
+
+compileTestJava {
+    sourceCompatibility = 17
+    targetCompatibility = 17
+}
 
-tasks.withType(Jar).configureEach {
+tasks.withType(Jar) {
     manifest {
         attributes 'Main-Class': 'picard.cmdline.PicardCommandLine',
                 'Implementation-Title': 'Picard',
@@ -131,9 +126,10 @@
     }
 }
 
-tasks.withType(Javadoc).configureEach {
+tasks.withType(Javadoc) {
     // do this for all javadoc tasks, including gatkDoc
     options.addStringOption('Xdoclint:none')
+    options.addStringOption('XDignore.symbol.file')
 }
 
 javadoc {
@@ -141,7 +137,7 @@
 }
 
 // Generate Picard Online Doc
-tasks.register('picardDoc', Javadoc) {
+task picardDoc(type: Javadoc) {
     dependsOn 'cleanPicardDoc', classes
     final File picardDocDir = file("build/docs/picarddoc")
     doFirst {
@@ -180,7 +176,7 @@
     options.noTimestamp(false)
 }
 
-tasks.register('currentJar', Copy) {
+task currentJar(type:  Copy) {
     from shadowJar
     into file("$buildDir/libs")
     rename { string -> "picard.jar" }
@@ -192,13 +188,13 @@
 
 // Run the tests using the legacy parser only. Assumes that test code is written using
 // legacy command line parser syntax.
-tasks.register('legacyTest', Test) {
+task legacyTest(type: Test) {
     systemProperty 'picard.useLegacyParser', 'true'
 }
 
 // Run the tests using the Barclay command line parser (useLegacyParser=false), which requires
 // conversion of test command lines from Picard-style command line syntax to Barclay-style syntax.
-tasks.register('barclayTest', Test) {
+task barclayTest(type: Test) {
     systemProperty 'picard.convertCommandLine', 'true'
 }
 
@@ -207,7 +203,7 @@
     dependsOn barclayTest
 }
 
-tasks.withType(Test).configureEach {
+tasks.withType(Test) {
     outputs.upToDateWhen { false } // tests will always rerun
     description = "Runs the unit tests"
 
@@ -235,6 +231,8 @@
         }
     }
 
+    maxParallelForks = 1
+
     // set heap size for the test JVM(s)
     minHeapSize = "1G"
     maxHeapSize = "2G"
@@ -273,126 +271,25 @@
     }
 }
 
-jacocoTestReport {
-    dependsOn legacyTest
-    group = "Reporting"
-    description = "Generate Jacoco coverage reports after running tests."
-    getAdditionalSourceDirs().from(sourceSets.main.allJava.srcDirs)
-
-    reports {
-        xml.required = true // coveralls plugin depends on xml format report
-        html.required = true
-    }
-}
-
 wrapper {
     gradleVersion = '8.5'
 }
 
-/**
- * Sign non-snapshot releases with our secret key.  This should never need to be invoked directly.
- */
-signing {
-    required { isRelease && gradle.taskGraph.hasTask("publish") }
-    sign publishing.publications
-}
-
-/**
- * Upload a release to sonatype.  You must be an authorized uploader and have your sonatype
- * username and password information in your gradle properties file.  See the readme for more info.
- *
- * For releasing to your local maven repo, use gradle install
- */
-publishing {
-    publications {
-        picard(MavenPublication) {
-            from components.java
-
-            pom {
-                name = 'Picard'
-                packaging = 'jar'
-                description = 'A set of command line tools (in Java) for manipulating high-throughput sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF.'
-                url = 'http://broadinstitute.github.io/picard/'
-
-                scm {
-                    url = 'git@github.com:broadinstitute/picard.git'
-                    connection = 'scm:git:git@github.com:broadinstitute/picard.git'
-                }
-
-                licenses {
-                    license {
-                        name = 'MIT License'
-                        url = 'http://opensource.org/licenses/MIT'
-                        distribution = 'repo'
-                    }
-                }
-
-                developers {
-                    developer {
-                        id = 'picard'
-                        name = 'Picard Team'
-                        url = 'http://broadinstitute.github.io/picard'
-                    }
-                }
-            }
-        }
-    }
-    repositories {
-        maven {
-            credentials {
-                username = isRelease ? project.findProperty("sonatypeUsername") : System.env.ARTIFACTORY_USERNAME
-                password = isRelease ? project.findProperty("sonatypePassword") : System.env.ARTIFACTORY_PASSWORD
-            }
-            def release = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
-            def snapshot = "https://broadinstitute.jfrog.io/broadinstitute/libs-snapshot-local/"
-            url = isRelease ? release : snapshot
-        }
-    }
-}
-
-publish {
-    doFirst {
-        System.out.println("Uploading version $version")
-    }
-}
-
 ext.htmlDir = file("build/docs/html")
 
 //update static web docs
-tasks.register('copyJavadoc', Copy) {
+task copyJavadoc(type: Copy) {
     dependsOn 'javadoc', 'picardDoc'
     from 'build/docs/javadoc'
     into "$htmlDir/javadoc"
 }
 
-tasks.register('copyPicardDoc', Copy) {
+task copyPicardDoc(type: Copy) {
     dependsOn 'picardDoc'
     from 'build/docs/picarddoc'
     into "$htmlDir/picarddoc"
 }
 
-tasks.register('updateGhPages') {
-    dependsOn 'copyJavadoc', 'copyPicardDoc'
-    outputs.dir htmlDir
-}
-
-updateGhPages.finalizedBy gitPublishPush
-
-gitPublish {
-    repoUri = 'git@github.com:broadinstitute/picard.git'
-    branch = 'gh-pages'
-    preserve { include '**/*' }
-    contents { 
-        from('build/docs/html') { 
-            into 'newdocs' 
-        } 
-    }
-}
-
-gitPublishCopy {
-    dependsOn 'updateGhPages', 'copyJavadoc', 'copyPicardDoc'
-}
-
 // For Gradle 8 explicitly add 'currentJar' as a dependency of the following tasks.
 // For more information, please refer to
 // https://docs.gradle.org/8.2.1/userguide/validation_problems.html#implicit_dependency in the Gradle documentation.
